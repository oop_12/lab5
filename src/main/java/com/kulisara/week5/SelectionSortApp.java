package com.kulisara.week5;

import javax.print.attribute.SetOfIntegerSyntax;

public class SelectionSortApp {
    public static void main(String[] args) {
        int arr[]=new int[1000];
        randomArray(arr);
        print(arr);
        selectionSort(arr);
        print(arr);
        }
    public static void print(int[] arr){
        for(int a: arr){
            System.out.print(a+" ");
        }
    System.out.println();
    }
    public static void randomArray(int arr[]){
        for(int i=0;i<arr.length;i++){
            arr[i]=(int)(Math.random()*10000);
        }

    }
    public static int findMinIndex(int[] arr, int pos) {
        int minIndex = pos;
        for (int i = pos + 1; i < arr.length; i++) {
            if (arr[minIndex] > arr[i]) {
                minIndex = i;
            }
        }
        return minIndex;
    }

    public static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void selectionSort(int[] arr) {
        
        int minIndex=0;
        for(int pos=0;pos<arr.length-1;pos++){
            minIndex=findMinIndex(arr,pos);
            swap(arr, minIndex, pos);
        }

    }

}
