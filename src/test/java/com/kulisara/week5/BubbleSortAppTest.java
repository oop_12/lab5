package com.kulisara.week5;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
public class BubbleSortAppTest {
    @Test
    public void shouldBubbleTestCase1(){
        int arr[]={5,4,3,2,1};
        int expected[]={4,3,2,1,5};
        int first =0;
        int second =4;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void shouldBubbleTestCase2(){
        int arr[]={4,3,2,1,5};
        int expected[]={3,2,1,4,5};
        int first =0;
        int second =3;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void shouldBubbleTestCase3(){
        int arr[]={3,2,1,4,5};
        int expected[]={2,1,3,4,5};
        int first =0;
        int second =2;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void shouldBubbleTestCase4(){
        int arr[]={2,1,3,4,5};
        int expected[]={1,2,3,4,5};
        int first =0;
        int second =1;
        BubbleSortApp.bubble(arr, first, second);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void shouldBubbleSortTestCase1(){
        int arr[]={5,4,3,2,1};
        int expected[]={1,2,3,4,5};
        
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(expected, arr);

    }
    @Test
    public void shouldBubbleSortTestCase2(){
        int arr[]={10,9,8,7,6,5,4,3,2,1};
        int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(sortedArr, arr);
    }
    @Test
    public void shouldBubbleSortTestCase3(){
        int arr[]={6,9,3,7,10,5,4,8,2,1};
        int sortedArr[] = {1,2,3,4,5,6,7,8,9,10};
        BubbleSortApp.bubbleSort(arr);
        assertArrayEquals(sortedArr, arr);
    }
}

